public class QotObj {
    final String startAuth=",\"author\":\"";
    final String endAuth="\",\"author_permalink\"";
    final String startBody="\",\"body\":\"";
    final String endBody=".\"}}";

    public String q;
    public String auth;

    static String getValue(String jsonString, String start, String end) {

        int startIndex = jsonString.indexOf(start) + start.length();
        int endIndex = jsonString.indexOf(end);
        return jsonString.substring(startIndex, endIndex);
    }
}
