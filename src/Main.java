import OS.Filesystem;
import OS.Name;
import YP.Type;
import org.w3c.dom.ls.LSOutput;

import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) throws IOException {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.
        System.out.printf("Hello and welcome!");


//        Урок 1. Введение в классы. Поля класса. Процедурное программирование, ООП,
//        организация кода
//        Цель задания:
//        Знакомство с такими парадигмами программирования, как ООП и процедурное
//        программирование, с  организацией кода, а также формирование базовых навыков для
//        работы с классами
//        Задание:
//        1.
//        Создайте классы с полями для описания структуры университета.
//        Комбинируйте!
//                Пусть одни классы будут полями других

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        Univer synergy = new Univer();
        synergy.predmets = new Predmet[10];
        for (int i = 0; i < 10; i++) {
            synergy.predmets[i] = new Predmet();
            synergy.predmets[i].topic = new Topic[20];
            for (int j = 0; j < 20; j++) {
                synergy.predmets[i].topic[j] = new Topic();
                synergy.predmets[i].topic[j].title = "Tema" + j;
            }
        }
//        2.
//        Создайте классы для описания операционных систем

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        OS.Name linux = new OS.Name();
        linux.title = "linux";
        linux.filesystems = new Filesystem[10];
        linux.filesystems[0] = new Filesystem();
        linux.filesystems[0].name = "ext4";

//        3.
//        Создайте классы для описания языков программирования (типизации, версии,
//                массив ключевых слов..)

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        YP.Name java = new YP.Name();
        java.title = "Java";
        java.types = new Type[8];
        java.version = "17";
        java.types[0] = new Type();
        java.types[0].name = "byte";


//        4.
//        Сохраните информацию о всех файлах в заданной директории в массив
//        FileI
//                nformation
//
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        File dir4 = new File("src/test/");
        System.out.println(dir4.getName());
        System.out.println(dir4.getAbsolutePath());
        File[] files4 = dir4.listFiles();
        System.out.println(dir4.listFiles());
        for (int i=0; i<files4.length;i++){
            System.out.println(files4[i].getName());
        }
        System.out.println("по какой то причине listFiles() возвращает null");
//        по какой то причине listFiles() возвращает null


//        5.
//        Сохраните снимок дня NASA в свой созданный класс


        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        String ur = "https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY";
        String jsonString = NasaDayPic.downloadWebPage(ur);
        System.out.println(jsonString);

        JSONObject jsonObject = new JSONObject(jsonString);

        NasaDayPic np = new NasaDayPic();
        np.link = jsonObject.getString("url");
        System.out.println("URL: " + np.link);


//        6.
//        Сделайте класс для цитаты из breaking bad: цитата и автор. Сохраните в массив
//        таких классов 10 цитат.


        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        String jsonString6 = NasaDayPic.downloadWebPage("https://favqs.com/api/qotd");
        System.out.println(jsonString6);
        System.out.println(jsonString6.length());

        QotObj q6 = new QotObj();

        q6.auth = QotObj.getValue(jsonString6, q6.startAuth, q6.endAuth);
        q6.q = QotObj.getValue(jsonString6, q6.startBody, q6.endBody);

        System.out.println("Author: " + q6.auth);
        System.out.println("Quote: " + q6.q);




//                Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написана общая структура про
//                граммы
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при усл
//        овии, что слушатель получил оценку не
//        менее 3 баллов


    }



}
